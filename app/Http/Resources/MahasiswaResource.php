<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MahasiswaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nama' => $this->nama,
            'nim' => $this->nim,
            'fakultas' => $this->fakultas,
            'jurusan' => $this->jurusan,
            'no_hp' => $this->no_hp,
            'no_wa' => $this->no_wa
        ];
    }
}
