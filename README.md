<h2>Panduan API perpustakaan</h2>
<p>Berikut ini merupakan panduan dari penggunaan API perpustakaan berupa "Route" yang digunakan, input data dan output data pada masing-masing "Route".</p>

Route untuk auth:
- localhost/api/register -> Untuk register akun. Dibutuhkan input "username", "email", dan "password".
- localhost/api/login -> Untuk login akun. Akan menghasilkan token auth.
- localhost/api/user -> Untuk menampilkan data "username" yang sedang login.
- localhost/api/logout -> Untuk logout akun.

Route untuk profile mahasiswa:
- localhost/api/profile -> Untuk input data mahasiswa. Dibutuhkan input "username" untuk generate "user_id", input data mahasiswa berupa "nama", "nim" yang unique, "fakultas", "jurusan", "no_hp", dan "no_wa" yang nullable.
- localhost/api/profile/edited -> Untuk update data mahasiswa. Dibutuhkan input "username" untuk generate "user_id", dan update data mahasiswa berupa "nama", "nim" yang unique, "fakultas", "jurusan", "no_hp", dan "no_wa" yang nullable.

Route untuk katalog buku:
- localhost/api/catalogue -> Untuk menampilkan semua buku yang ada.
- localhost/api/catalogue/{kode_buku} -> Untuk menampilkan buku dengan data "kode" adalah "kode_buku".
- localhost/api/catalogue/new -> Untuk membuat data buku baru. <strong>Hanya Admin yang dapat akses.</strong> Dibutuhkan input "kode", "judul", "pengarang", dan "tahun_terbit" dengan format "YYYY".
- localhost/api/catalogue/{kode_buku}/edited -> Untuk update data buku dengan data "kode" adalah "kode_buku". <strong>Hanya Admin yang dapat akses.</strong> Dibutuhkan input "kode", "judul", "pengarang", dan "tahun_terbit" dengan format "YYYY".
- localhost/api/catalogue/{kode_buku}/deleted -> Untuk hapus data buku dengan data "kode" adalah "kode_buku". <strong>Hanya Admin yang dapat akses.</strong>

Route untuk peminjaman dan pengembalian buku:
- localhost/api/borrow -> Untuk generate data buku yang dipinjam oleh user. Dibutuhkan input "kode" yang merupakan kode buku dan "username" yang meminjam. Controller akan otomatis menggenerate "waktu_peminjaman" dengan waktu saat input data, dan "batas_pengembalian" <strong> yang diatur 7 hari setelah "waktu_peminjaman".</strong> "status_pengembalian" secara default bernilai <b>false</b>. Terakhir, controller akan menggenerate random "token_pengembalian" sebagai input verifikasi pengembalian.
- localhost/api/return -> Untuk update data buku yang dipinjam oleh user, memberi data bahwa buku sudah dikembalikan. <strong>Hanya Admin yang dapat akses.</strong> Dibutuhkan input "token_pengembalian" untuk mencari data peminjaman. Controller akan otomatis menggenerate "waktu_pengembalian" dengan waktu saat input data, dan merubah "status_pengembalian" menjadi <b>true</b>. Controller akan membandingkan "waktu_pengembalian" dengan "batas_pengembalian". Jika lebih kecil (waktu pengembalian kurang dari batas waktu pengembalian), "status_ontime" akan menjadi <b>true</b>, dan sebaliknya.
